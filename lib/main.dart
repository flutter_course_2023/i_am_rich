import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: Scaffold(
        appBar: AppBar(
          title: Center(
              child: Text('I am Rich')
          ),
          backgroundColor: Colors.blueGrey[800],
        ),

        body: Center(
          child: Image.asset('images/Diamond.png',
            width: 330,
          ),
        ),

        backgroundColor: Colors.blueGrey
      ),

    );
  }

}

